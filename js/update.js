function updateelement(referencia)
{
  //mostramos los componentes al modal a cargar un nuevo empleado
  document.getElementById('myModalLabel').innerHTML="Modificar Empleado";
  document.getElementById('modalbody').innerHTML="<table class='table'>"+
  "<thead><tr><td>Nombre</td><td>Apellido Paterno</td><td>Apellido Materno</td>"+
  "<td>Direccion</td><td>Correo Electronico</td>"+
  "</tr></thead>"+
  "<tbody>"+
  "<tr>"+
  "<td><input type='text' class='form-control' id='nombre'></td>"+
  "<td><input type='text' class='form-control' id='apelli_pat'></td>"+
  "<td><input type='text' class='form-control' id='apelli_mat'></td>"+
  "<td><input type='text' class='form-control' id='direcc'></td>"+
  "<td><input type='text' class='form-control' id='correo'></td>"+
  "</tr>"+
  "</tbody></table>";
  document.getElementById('modalFooter').innerHTML=
  "<button class='btn btn-primary' id='guardar' onclick='saveload("+referencia+")'>Actualizar</button>"+
  "<button class='btn btn-danger' data-dismiss='modal' aria-label='Close'>Cerrar</button>";

  //rellenamos las variables
  var xmlhttp = new XMLHttpRequest();
  var url = "code/Empleado.php";
  var data =1;
  var vars = 'identif=5&id='+referencia;

  xmlhttp.onreadystatechange=function() {
      if (this.readyState == 4 && this.status == 200) {
          myFunctionsetsave(this.responseText);
      }
  }
  xmlhttp.open("POST", url, true);
  xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xmlhttp.send(vars);
}

function myFunctionsetsave(response)
{
    var arr = JSON.parse(response);
    var i;

    for(i = 0; i < arr.length; i++) {
      document.getElementById('nombre').value= arr[i].Nombre;
      document.getElementById('apelli_pat').value= arr[i].Apellido_Pat;
      document.getElementById('apelli_mat').value= arr[i].Apellido_Mat;
      document.getElementById('direcc').value= arr[i].Direccion;
      document.getElementById('correo').value= arr[i].Correo;
    }
}







/////precargar la acualizacion al sistema
function saveload(refere)
{
  var nombre = document.getElementById('nombre').value;
  var apellidopat = document.getElementById('apelli_pat').value;
  var apellidomat = document.getElementById('apelli_mat').value;
  var direccion = document.getElementById('direcc').value;
  var correo = document.getElementById('correo').value;

  var xmlhttp = new XMLHttpRequest();
  var url = "code/Empleado.php";
  var data =1;
  var vars = 'identif=6&idref='+refere+'&nombre='+nombre+'&apellidopa='+apellidopat+'&apellidomat='+apellidomat+'&direccion='+direccion+'&correo='+correo;

  xmlhttp.onreadystatechange=function() {
      if (this.readyState == 4 && this.status == 200) {
          modificarnuevo(this.responseText);
      }
  }
  xmlhttp.open("POST", url, true);
  xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xmlhttp.send(vars);

}
function modificarnuevo(response)
{
    var arr = JSON.parse(response);
    alert(arr);
    autorecargar();
}
